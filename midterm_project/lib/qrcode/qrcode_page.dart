import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:midterm_project/home_page.dart';

class QrcodePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return QrcodePageState();
  }
}

class QrcodePageState extends State<QrcodePage> {
  @override
  Widget build(BuildContext context) {
    debugShowCheckedModeBanner:
    false;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            color: Colors.black,
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => homepage()));
            },
          ),
          title: Text(
            'Tich Diem',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: Container(
          padding: EdgeInsets.only(top: 100),
          width: double.infinity,
          height: double.infinity,
          child: Column(children: [
            Center(
              child: QrImage(
                backgroundColor: Color.fromARGB(255, 0, 0, 0),
                foregroundColor: Colors.white,
                embeddedImage: NetworkImage(
                    'https://cdn.haitrieu.com/wp-content/uploads/2021/11/Logo-The-Gioi-Di-Dong-MWG.png'),
                data: "1234567890",
                version: QrVersions.auto,
                size: 250.0,
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 30)),
            Center(
              child: Text(
                'Đưa mã này cho nhân viên',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Container(
                height: 100,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          child: MaterialButton(
                              onPressed: () {},
                              color: Color.fromARGB(255, 255, 255, 255),
                              child: RichText(
                                text: const TextSpan(
                                  children: [
                                    WidgetSpan(
                                      child: Icon(
                                        Icons.qr_code,
                                        size: 25,
                                        color: Colors.black,
                                      ),
                                    ),
                                    TextSpan(
                                        text: "Mã Định Danh ",
                                        style: TextStyle(color: Colors.black)),
                                  ],
                                ),
                              ))),
                      Expanded(
                          child: MaterialButton(
                              onPressed: () {},
                              color: Color.fromARGB(255, 255, 255, 255),
                              child: RichText(
                                text: const TextSpan(
                                  children: [
                                    WidgetSpan(
                                      child: Icon(
                                        Icons.qr_code_scanner,
                                        size: 25,
                                        color: Colors.black,
                                      ),
                                    ),
                                    TextSpan(
                                        text: "Quét Tích Điểm ",
                                        style: TextStyle(color: Colors.black)),
                                  ],
                                ),
                              ))),
                    ]))
          ]),
        ));
  }
}
