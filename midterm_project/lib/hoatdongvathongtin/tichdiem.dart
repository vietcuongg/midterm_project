import 'package:flutter/material.dart';
import 'package:midterm_project/menu/navbar.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:midterm_project/home_page.dart';

import '../navigationmenu/navbar.dart';

class tichdiempage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return tichdiempageState();
  }
}

class tichdiempageState extends State<tichdiempage> {
  @override
  Widget build(BuildContext context) {
    debugShowCheckedModeBanner:
    false;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            color: Colors.black,
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => homepage()));
            },
          ),
          title: Text(
            'Quy định tích điểm ,sủ dụng điểm',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Center(
                  child: Row(
                    children: [
                      Padding(padding: EdgeInsets.all(40)),
                      Text('My Qùa Tặng VIIP',
                          style: TextStyle(
                              color: Colors.orange,
                              fontSize: 35,
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Text('QUY ĐỊNH TÍCH ĐIỂM, ĐỔI ĐIÊM VÀ  ',
                        style: TextStyle(
                            color: Colors.green,
                            fontSize: 15,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Text('CHĂM SÓC KHÁCH HÀNG',
                        style: TextStyle(
                            color: Colors.green,
                            fontSize: 15,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Text('TẢI ỨNG DỤNG "MY QUA TANG VIP"',
                        style: TextStyle(
                            color: Colors.green,
                            fontSize: 15,
                            fontWeight: FontWeight.bold))
                  ],
                ),
                SizedBox(
                  height: 18,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text('1. QUY ĐỊNH CHUNG',
                              style: TextStyle(
                                  color: Color.fromARGB(255, 0, 16, 104),
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold))
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'a. Chính sách này áp dụng chung cho tất cả khách hàng sử dụng ứng dụng Quà Tặng VIP của Công Ty TNHH MTV Công Nghệ Thông Tin Thế Giới Di Động (“CNTT”). Chính sách riêng của từng đối tác sẽ được thông báo riêng tùy từng thời điểm và do đối tác chịu trách nhiệm thực hiện.'))
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'b. Đối tác: là các thương nhân đang có hợp đồng cung cấp dịch vụ thương mại điện tử với CNTT còn hiệu lực và là bên yêu cầu CNTT tích điểm hoặc thực hiện các hoạt động chăm sóc khách hàng, bao gồm không giới hạn tặng quà, phiếu mua hàng hoặc các quyền lợi khác cho khách hàng.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'c.Giao dịch áp dụng tích điểm: toàn bộ các giao dịch (tại cửa hàng hoặc trực tuyến trên toàn quốc) mà Đối tác đã xác nhận thành công (khách hàng đã thanh toán đầy đủ và đã nhận được hàng) và không thực hiện trả hàng hóa/dịch vụ theo các chính sách trả hàng mà Đối tác đã công bố.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'd. Tỷ lệ tích điểm: là tỷ lệ phần trăm được CNTT xác định theo từng nhóm sản phẩm hoặc dịch vụ của từng Đối tác, mà theo đó điểm được xác định theo Tỷ lệ tích điểm nhân với giá trị hàng hóa/dịch vụ thực trả (bao gồm VAT, không bao gồm khuyến mại khác) theo Giao dịch áp dụng tích điểm.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'e. Tỷ lệ quy đổi là một điểm tương đương một đồng áp dụng cho cả chính sách tích điểm và chính sách sử dụng điểm căn cứ theo Tỷ lệ tích điểm đã công bố.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'f. Mọi khiếu nại liên quan đến tích điểm hoặc sử dụng điểm sẽ theo chính sách này hoặc chính sách riêng tùy từng thời điểm, quyết định cuối cùng do CNTT quyết định trong mọi trường hợp.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text('2. CHÍNH SÁCH TÍCH ĐIỂM',
                              style: TextStyle(
                                  color: Color.fromARGB(255, 0, 16, 104),
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold))
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'a. . Điều kiện: (i) Hàng hóa đã được giao thành công cho Khách hàng; (ii) Khách hàng đã thanh toán đủ 100% giá trị đơn hàng; (iii) Đã xác thực thông tin mua hàng đúng với tài khoản đã đăng ký tại Ứng Dụng và (iv) không thuộc các giao dịch không được tích điểm.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'b. Khách hàng tích điểm sau đó: Khách hàng sử dụng Ứng dụng và quét mã QR code hoặc nhập mã tích điểm trên hóa đơn/biên nhận. CNTT không chịu trách nhiệm khi Khách hàng không sử dụng hóa đơn/biên lai để tích điểm sau thời gian này. (Hóa đơn chưa được tích điểm mới xuất hiện mã QR và mã tích điểm)')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'c. Điểm được tích lũy ngay khi khách hàng hoàn tất bước b. nêu trên và có thể sử dụng điểm sau khi được hệ thống xác nhận tích điểm thành công sau 48 giờ kể từ thời điểm tích điểm.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'd. Thời hạn của điểm: Điểm có thời gian sử dụng 12 tháng tính từ thời điểm tích điểm thành công. Nếu khách hàng không sử dụng trong vòng 12 tháng thì hệ thống sẽ tự động trừ đi số điểm hết hạn, hệ thống sẽ thông báo đến Khách hàng trước 30 (ba mươi) ngày.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'e. CNTT được toàn quyền không tích điểm cho các Giao dịch không được tích điểm và khấu trừ số điểm được tích không hợp lệ do các giao dịch đó phát sinh mà không cần bất kỳ sự đồng ý hoặc chấp thuận nào từ người có liên quan.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'f. Trường hợp khách hàng đăng ký tham gia chương trình tích điểm theo nhóm gia đình thì chính sách tích điểm áp dụng riêng và sẽ được công bố bởi CNTT tùy từng thời điểm.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
