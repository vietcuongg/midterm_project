import 'package:flutter/material.dart';
import 'package:midterm_project/menu/navbar.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:midterm_project/home_page.dart';

import '../navigationmenu/navbar.dart';

class quychehoatdongPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return quychehoatdongPageState();
  }
}

class quychehoatdongPageState extends State<quychehoatdongPage> {
  @override
  Widget build(BuildContext context) {
    debugShowCheckedModeBanner:
    false;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            color: Colors.black,
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => homepage()));
            },
          ),
          title: Text(
            'QUY CHẾ HOẠT ĐỘNG',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Center(
                  child: Row(
                    children: [
                      Padding(padding: EdgeInsets.all(40)),
                      Text('My Qùa Tặng VIIP',
                          style: TextStyle(
                              color: Colors.orange,
                              fontSize: 35,
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Text('QUY CHẾ HOẠT ĐỘNG ỨNG  ',
                        style: TextStyle(
                            color: Color.fromARGB(255, 0, 28, 80),
                            fontSize: 15,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Text('DỤNG CUNG CẤP DỊCH VỤ',
                        style: TextStyle(
                            color: Color.fromARGB(255, 0, 28, 80),
                            fontSize: 15,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Text(' ỨNG DỤNG "MY QUA TANG VIP"',
                        style: TextStyle(
                            color: Color.fromARGB(255, 0, 28, 80),
                            fontSize: 15,
                            fontWeight: FontWeight.bold))
                  ],
                ),
                SizedBox(
                  height: 18,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text('1. NGUYÊN TẮC CHUNG',
                              style: TextStyle(
                                  color: Color.fromARGB(255, 0, 16, 104),
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold))
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'a. Ứng Dụng cung cấp dịch vụ thương mại điện tử Quà Tặng VIP (“Ứng Dụng”) là Ứng Dụng do Công Ty TNHH MTV Công Nghệ Thông Tin Thế Giới Di Động (“CNTT”) sở hữu, trực tiếp quản lý và vận hành với mục tiêu, chức năng chính là thay mặt các thương nhân thực hiện khuyến mại, chăm sóc khách hàng bằng việc tổ chức chương trình khách hàng thường xuyên, theo đó việc tặng thưởng cho khách hàng căn cứ trên số lượng hoặc trị giá mua hàng hóa, dịch vụ mà khách hàng thực hiện dưới hình thức tích điểm hoặc đổi điểm để nhận phiếu điểm để mua hàng hóa, dịch vụ khác trên Ứng Dụng.'))
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'b. Khách hàng mua hàng hóa hoặc dịch vụ của các thương nhân có đăng ký chương trình Quà tặng VIP với CNTT (“Đối tác”), khách hàng sẽ được ghi nhận một số điểm tích lũy tương ứng dựa trên giá trị mỗi đơn hàng mà khách hàng đã mua vào tài khoản của khách hàng tại Ứng Dụng. Khách hàng có thể sử dụng điểm tích lũy để đối phiếu quà tặng có giá trị tương ứng bằng tiền để mua hàng hóa, dịch vụ của chính Đối tác đó hoặc Đối tác khác có đăng ký trên Ứng Dụng. Chính sách tích điểm theo giá trị đơn hàng đã thanh toán và/hoặc chính sách đổi điểm thành phiếu quà tặng phụ thuộc vào quyết định của Thương nhân và/hoặc CNTT qua từng thời điểm.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'c.Thông qua Ứng Dụng, Người dùng tiến hành mở tài khoản và đăng ký thẻ khách hàng thường xuyên/khách hàng thân thiết (thẻ dạng điện tử được hiển thị dưới dạng QR code) để được sử dụng hàng hoá, dịch vụ từ một hệ thống các đối tác với giá ưu đãi hoặc hưởng những lợi ích khác so với khi mua hàng hoá, dịch vụ tại từng đối tác riêng lẻ. Việc đăng ký trở thành khách hàng thường xuyên tại thời điểm hiện tại chúng tôi chưa thực hiện thu phí dịch vụ.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'd. Tất cả các nội dung tại Quy chế này phải tuân thủ theo hệ thống pháp luật hiện hành của Việt Nam. Khách hàng, Đối tác khi tham gia vào Ứng Dụng phải hiểu trách nhiệm pháp lý của mình, tuân thủ quy định pháp luật và nội dung tại Quy chế; (các) chính sách, (các) điều khoản và điều kiện riêng của từng Đối tác được được công khai thông báo tại Ứng Dụng tại từng thời điểm.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'e. Tỷ lệ quy đổi là một điểm tương đương một đồng áp dụng cho cả chính sách tích điểm và chính sách sử dụng điểm căn cứ theo Tỷ lệ tích điểm đã công bố.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'f. Mọi khiếu nại liên quan đến tích điểm hoặc sử dụng điểm sẽ theo chính sách này hoặc chính sách riêng tùy từng thời điểm, quyết định cuối cùng do CNTT quyết định trong mọi trường hợp.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text('2. CHÍNH SÁCH TÍCH ĐIỂM',
                              style: TextStyle(
                                  color: Color.fromARGB(255, 0, 16, 104),
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold))
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'a. . Điều kiện: (i) Hàng hóa đã được giao thành công cho Khách hàng; (ii) Khách hàng đã thanh toán đủ 100% giá trị đơn hàng; (iii) Đã xác thực thông tin mua hàng đúng với tài khoản đã đăng ký tại Ứng Dụng và (iv) không thuộc các giao dịch không được tích điểm.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'b. Khách hàng tích điểm sau đó: Khách hàng sử dụng Ứng dụng và quét mã QR code hoặc nhập mã tích điểm trên hóa đơn/biên nhận. CNTT không chịu trách nhiệm khi Khách hàng không sử dụng hóa đơn/biên lai để tích điểm sau thời gian này. (Hóa đơn chưa được tích điểm mới xuất hiện mã QR và mã tích điểm)')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'c. Điểm được tích lũy ngay khi khách hàng hoàn tất bước b. nêu trên và có thể sử dụng điểm sau khi được hệ thống xác nhận tích điểm thành công sau 48 giờ kể từ thời điểm tích điểm.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'd. Thời hạn của điểm: Điểm có thời gian sử dụng 12 tháng tính từ thời điểm tích điểm thành công. Nếu khách hàng không sử dụng trong vòng 12 tháng thì hệ thống sẽ tự động trừ đi số điểm hết hạn, hệ thống sẽ thông báo đến Khách hàng trước 30 (ba mươi) ngày.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'e. CNTT được toàn quyền không tích điểm cho các Giao dịch không được tích điểm và khấu trừ số điểm được tích không hợp lệ do các giao dịch đó phát sinh mà không cần bất kỳ sự đồng ý hoặc chấp thuận nào từ người có liên quan.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.all(30)),
                          Expanded(
                              child: Text(
                                  'f. Trường hợp khách hàng đăng ký tham gia chương trình tích điểm theo nhóm gia đình thì chính sách tích điểm áp dụng riêng và sẽ được công bố bởi CNTT tùy từng thời điểm.')),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
