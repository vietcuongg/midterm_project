import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:midterm_project/bottom_menu/lichsu.dart';
import 'package:midterm_project/bottom_menu/taikhoan.dart';
import 'package:midterm_project/hoatdongvathongtin/quychehoatdong.dart';
import 'package:midterm_project/hoatdongvathongtin/tichdiem.dart';
import 'package:midterm_project/menu/gopykhieunai.dart';
import 'package:midterm_project/qrcode/qrcode_page.dart';
import 'package:midterm_project/home_page.dart';
import 'package:midterm_project/bottom_menu/lichsu.dart';

import '../menu/vesinhthietbi.dart';

class NavBar extends StatelessWidget {
  final Padding = EdgeInsets.symmetric(horizontal: 250);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(
        child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50.0),
      ),
      padding: EdgeInsets.only(
          top: MediaQuery.of(context).padding.top,
          left: MediaQuery.of(context).padding.left),
      child: Column(
        children: [
          ListTile(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios_outlined),
              color: Colors.black,
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => homepage()));
              },
            ),
            title: Text(
              'MENU',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Color.fromARGB(255, 2, 0, 95)),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.75,
            decoration:
                BoxDecoration(color: Color.fromARGB(255, 255, 255, 255)),
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  children: [
                    InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => QrcodePage()));
                        },
                        child: const ListTile(
                          title: Text('Hoạt động và thông tin của tôi',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15)),
                          dense: true,
                          visualDensity: VisualDensity(vertical: -2.5),
                        )),
                    InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LichSu()));
                        },
                        child: const ListTile(
                          minLeadingWidth: 0.05,
                          title: Text('Lịch sử hoạt động',
                              style: TextStyle(
                                  color: Color.fromARGB(212, 51, 50, 50),
                                  fontSize: 12)),
                          leading: Icon(
                            Icons.watch_later_outlined,
                            color: Color.fromARGB(255, 0, 1, 70),
                          ),
                          dense: true,
                          visualDensity: VisualDensity(vertical: -4),
                        )),
                    InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => QrcodePage()));
                        },
                        child: const ListTile(
                          minLeadingWidth: 0.05,
                          title: Text('Liên kết tài khoản',
                              style: TextStyle(
                                  color: Color.fromARGB(212, 51, 50, 50),
                                  fontSize: 12)),
                          leading: Icon(
                            Icons.credit_card,
                            color: Color.fromARGB(255, 0, 1, 70),
                          ),
                          dense: true,
                          visualDensity: VisualDensity(vertical: -4),
                        )),
                    InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => taikhoanpage()));
                        },
                        child: const ListTile(
                          minLeadingWidth: 0.05,
                          title: Text('Quản lí tài khoản',
                              style: TextStyle(
                                  color: Color.fromARGB(212, 51, 50, 50),
                                  fontSize: 12)),
                          leading: Icon(
                            Icons.account_balance_wallet,
                            color: Color.fromARGB(255, 0, 1, 70),
                          ),
                          dense: true,
                          visualDensity: VisualDensity(vertical: -4),
                        )),
                    InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => QrcodePage()));
                        },
                        child: const ListTile(
                          minLeadingWidth: 0.05,
                          title: Text('Đăng xuất',
                              style: TextStyle(
                                color: Color.fromARGB(255, 255, 17, 0),
                                fontSize: 12,
                              )),
                          leading: Icon(
                            Icons.logout_outlined,
                            color: Color.fromARGB(255, 255, 17, 0),
                          ),
                          dense: true,
                          visualDensity: VisualDensity(vertical: -4),
                        )),
                    Divider(
                      height: 0.1,
                      thickness: 0.5,
                      indent: 1,
                      color: Colors.black,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.26,
                      margin: EdgeInsets.all(10),
                      width: double.infinity,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Expanded(
                                  child: Text(
                                "Các tiện ích và hỗ trợ nhanh",
                                style: TextStyle(
                                    fontSize: 18.0,
                                    color: Color.fromARGB(255, 0, 60, 90),
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.bold),
                              ))
                            ],
                          ),
                          Container(
                            child: GridView(
                              shrinkWrap: true,
                              primary: true,
                              padding: EdgeInsets.only(top: 10),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                crossAxisSpacing: 10,
                                mainAxisSpacing: 10,
                                mainAxisExtent: 40,
                              ),
                              children: <Widget>[
                                Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      MaterialButton(
                                        height: 20,
                                        minWidth: 10,
                                        color: Colors.orange,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20.0)),
                                        child: Icon(
                                          Icons.badge_outlined,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                        onPressed: () {},
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Expanded(
                                        child: Text(
                                          'Theo goi don hang ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      MaterialButton(
                                        height: 20,
                                        minWidth: 10,
                                        color: Color.fromARGB(255, 2, 76, 145),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20.0)),
                                        child: Icon(
                                          Icons.auto_fix_high,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                        onPressed: () {},
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child: Text(
                                          'Hỗ trợ bảo hành sửa chữa  ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      MaterialButton(
                                        height: 20,
                                        minWidth: 10,
                                        color:
                                            Color.fromARGB(255, 28, 243, 214),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20.0)),
                                        child: const Icon(
                                          Icons.message_outlined,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      gopukhieunaiPage()));
                                        },
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child: Text(
                                          'Góp ý - khiếu nại ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      MaterialButton(
                                        height: 20,
                                        minWidth: 10,
                                        color:
                                            Color.fromARGB(212, 245, 102, 238),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20.0)),
                                        child: Icon(
                                          Icons.perm_phone_msg,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                        onPressed: () {},
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child: Text(
                                          'Hỗ trợ kĩ thuật trực tuyến  ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      MaterialButton(
                                        height: 20,
                                        minWidth: 10,
                                        color:
                                            Color.fromARGB(255, 94, 238, 118),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20.0)),
                                        child: Icon(
                                          Icons.calendar_today,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      vesinhthietbiPage()));
                                        },
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child: Text(
                                          'Đặt lịch vệ sinh thiết bị ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      MaterialButton(
                                        height: 20,
                                        minWidth: 10,
                                        color: Color.fromARGB(255, 231, 58, 58),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20.0)),
                                        child: Icon(
                                          Icons.card_giftcard_sharp,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                        onPressed: () {},
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child: Text(
                                          'Qùa của tôi ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    DottedBorder(
                      color: Colors.orange,
                      strokeWidth: 1.25,
                      dashPattern: [
                        4,
                        5,
                      ],
                      child: Container(
                        color: Color.fromARGB(223, 253, 255, 148),
                        width: 220,
                        padding: const EdgeInsets.all(5.0),
                        child: const Text(
                          "Tổng đài hỗ trợ(gọi miễn phí): 1800.1060",
                          style: TextStyle(
                              fontSize: 10.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(
                      height: 0.1,
                      thickness: 1,
                      indent: 1,
                      color: Colors.black,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.4,
                      color: Color.fromARGB(255, 255, 255, 255),
                      child: Column(
                        children: [
                          InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => QrcodePage()));
                              },
                              child: const ListTile(
                                title: Text('Hoạt động và thông tin của tôi',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15)),
                                dense: true,
                                visualDensity: VisualDensity(vertical: -2.5),
                              )),
                          InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => tichdiempage()));
                              },
                              child: const ListTile(
                                minLeadingWidth: 0.05,
                                title: Text('Quy định tích điểm, sử dụng điểm',
                                    style: TextStyle(
                                        color: Color.fromARGB(212, 51, 50, 50),
                                        fontSize: 12)),
                                leading: Icon(
                                  Icons.card_membership,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                                dense: true,
                                visualDensity: VisualDensity(vertical: -4),
                              )),
                          InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            quychehoatdongPage()));
                              },
                              child: const ListTile(
                                minLeadingWidth: 0.05,
                                title: Text('Quy chế hoạt động',
                                    style: TextStyle(
                                        color: Color.fromARGB(212, 51, 50, 50),
                                        fontSize: 12)),
                                leading: Icon(
                                  Icons.card_membership,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                                dense: true,
                                visualDensity: VisualDensity(vertical: -4),
                              )),
                          InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => QrcodePage()));
                              },
                              child: const ListTile(
                                minLeadingWidth: 0.05,
                                title: Text('Thông tin đối tác ',
                                    style: TextStyle(
                                        color: Color.fromARGB(212, 51, 50, 50),
                                        fontSize: 12)),
                                leading: Icon(
                                  Icons.card_membership,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                                dense: true,
                                visualDensity: VisualDensity(vertical: -4),
                              )),
                          InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => QrcodePage()));
                              },
                              child: const ListTile(
                                minLeadingWidth: 0.05,
                                title: Text('Thông tin khuyến mãi   ',
                                    style: TextStyle(
                                        color: Color.fromARGB(212, 51, 50, 50),
                                        fontSize: 12)),
                                leading: Icon(
                                  Icons.card_membership,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                                dense: true,
                                visualDensity: VisualDensity(vertical: -4),
                              )),
                          InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => QrcodePage()));
                              },
                              child: const ListTile(
                                minLeadingWidth: 0.05,
                                title: Text('Thông tin chủ sở hữu ',
                                    style: TextStyle(
                                        color: Color.fromARGB(212, 51, 50, 50),
                                        fontSize: 12)),
                                leading: Icon(
                                  Icons.card_membership,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                                dense: true,
                                visualDensity: VisualDensity(vertical: -4),
                              )),
                          InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => QrcodePage()));
                              },
                              child: const ListTile(
                                minLeadingWidth: 0.05,
                                title: Text('Cập nhật ứng dụng 1.0.8 ',
                                    style: TextStyle(
                                        color: Color.fromARGB(212, 51, 50, 50),
                                        fontSize: 12)),
                                leading: Icon(
                                  Icons.arrow_circle_up,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                                dense: true,
                                visualDensity: VisualDensity(vertical: -4),
                              )),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
