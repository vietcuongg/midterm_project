import 'package:flutter/material.dart';
import 'package:midterm_project/home_page.dart';
import 'package:midterm_project/main.dart';
import 'package:midterm_project/screens/introduction_screens.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'QuaTangVip',
      theme: ThemeData(),
      home: IntroPage(),
    );
  }
}
