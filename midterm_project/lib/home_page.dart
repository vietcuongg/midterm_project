import 'package:flutter/material.dart';
import 'package:midterm_project/bottomTabBar/history_page.dart';
import 'package:midterm_project/bottomTabBar/profile.dart';
import 'package:midterm_project/bottomTabBar/wellcome_page.dart';
import 'package:midterm_project/bottom_menu/lichsu.dart';
import 'package:midterm_project/bottom_menu/taikhoan.dart';
import 'package:midterm_project/bottom_menu/xemdiem.dart';
import 'package:midterm_project/qrcode/qrcode_page.dart';
import 'bottomTabBar/history_page.dart';
import 'bottomTabBar/score_page.dart';
import 'bottomTabBar/wellcome_page.dart';
import 'qrcode/qrcode_page.dart';

class homepage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return homepageState();
  }
}

class homepageState extends State<homepage> {
  int currentTab = 0;
  final List<Widget> screens = [
    wellcomepage(),
    historypage(),
    scorepage(),
    historypage()
  ];

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = wellcomepage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(
          Icons.qr_code_scanner_outlined,
          color: Color.fromARGB(255, 255, 230, 0),
        ),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => QrcodePage()));
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = wellcomepage();
                        currentTab = 0;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.home,
                          color: currentTab == 0 ? Colors.orange : Colors.black,
                        ),
                        Text(
                          'Home page',
                          style: TextStyle(
                              color: currentTab == 0
                                  ? Colors.orange
                                  : Colors.black),
                        )
                      ],
                    ),
                  ),
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => XemDiemPage()));
                        currentScreen = XemDiemPage();
                        currentTab = 1;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.credit_card_outlined,
                          color: currentTab == 1 ? Colors.orange : Colors.black,
                        ),
                        Text(
                          'Xem Diem',
                          style: TextStyle(
                              color: currentTab == 1
                                  ? Colors.orange
                                  : Colors.black),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        // Navigator.push(context,
                        //     MaterialPageRoute(builder: (context) => LichSu()));
                        currentScreen = LichSu();
                        currentTab = 2;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.home,
                          color: currentTab == 2 ? Colors.orange : Colors.black,
                        ),
                        Text(
                          'Lich Su',
                          style: TextStyle(
                              color: currentTab == 2
                                  ? Colors.orange
                                  : Colors.black),
                        )
                      ],
                    ),
                  ),
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => taikhoanpage()));
                        currentScreen = taikhoanpage();
                        currentTab = 3;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.person_outline,
                          color: currentTab == 3 ? Colors.orange : Colors.black,
                        ),
                        Text(
                          'Tai Khoan',
                          style: TextStyle(
                              color: currentTab == 3
                                  ? Colors.orange
                                  : Colors.black),
                        )
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
