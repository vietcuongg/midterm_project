import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:midterm_project/home_page.dart';
import 'package:midterm_project/bottom_menu/lichsu.dart';

class taikhoanpage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return taikhoanpageState();
  }
}

class taikhoanpageState extends State<taikhoanpage> {
  String radioButtonItem = 'ONE';
  int id = 1;

  @override
  Widget build(BuildContext context) {
    var pencil_alt;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          color: Colors.black,
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => homepage()));
          },
        ),
        title: Text(
          'Lịch sử điểm',
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Container(
                width: 60,
                child: CircleAvatar(
                  backgroundColor: Colors.orange,
                  radius: 50,
                  child: Icon(
                    Icons.person_add,
                    size: 30,
                    color: Colors.white,
                  ),
                ),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: Color.fromARGB(255, 0, 24, 158),
                    width: 3.0,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
              child: Text(
                'Profile',
                style: TextStyle(
                    color: Color.fromARGB(255, 0, 35, 63),
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
            ),
            Center(
              child: Container(
                padding: EdgeInsets.only(left: 120, top: 1),
                color: Color.fromARGB(255, 255, 255, 255),
                child: Column(
                  children: [
                    InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LichSu()));
                        },
                        child: const ListTile(
                          minLeadingWidth: 0.05,
                          title: Text('Đăng Xuất',
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold)),
                          leading: Icon(
                            Icons.logout_outlined,
                            color: Colors.red,
                          ),
                          dense: true,
                          visualDensity: VisualDensity(vertical: -4),
                        )),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              height: MediaQuery.of(context).size.height * 1,
              width: 500,
              child: Column(
                children: [
                  Row(
                    children: [
                      Padding(padding: EdgeInsets.all(10)),
                      Text('Trang cá nhân'),
                    ],
                  ),
                  Row(children: [
                    Padding(padding: EdgeInsets.all(10)),
                    Text(
                      'Danh Xưng:',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 0, 21, 37)),
                    ),
                    Radio(
                      value: 1,
                      groupValue: id,
                      onChanged: (val) {
                        setState(() {
                          radioButtonItem = 'Anh';
                          id = 1;
                        });
                      },
                    ),
                    Text(
                      'Anh',
                      style: new TextStyle(
                        fontSize: 15,
                        color: Colors.blueAccent,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Hay',
                      style: new TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                      ),
                    ),
                    Radio(
                      value: 2,
                      groupValue: id,
                      onChanged: (val) {
                        setState(() {
                          radioButtonItem = 'Chị';
                          id = 2;
                        });
                      },
                    ),
                    Text(
                      'Chị',
                      style: new TextStyle(
                        fontSize: 15,
                        color: Colors.blueAccent,
                      ),
                    ),
                  ]),
                  Divider(
                    height: 0.1,
                    thickness: 1,
                    indent: 1,
                    color: Colors.black,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Padding(padding: EdgeInsets.all(10)),
                      Text(
                        'Họ và tên',
                        style: TextStyle(
                            color: Color.fromARGB(255, 0, 21, 37),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      TextField(
                        decoration: InputDecoration(
                          hintText: 'Enter your name',
                          hintStyle: TextStyle(fontSize: 14),
                          contentPadding: EdgeInsets.only(left: 20.0, top: 5),
                          suffixIcon: IconButton(
                            onPressed: () {},
                            icon: Icon(Icons.mode_edit_outline),
                          ),
                        ),
                      )
                    ],
                  ),
                  Divider(
                    height: 0.1,
                    thickness: 1,
                    indent: 1,
                    color: Colors.black,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Padding(padding: EdgeInsets.all(10)),
                      Text(
                        'Số điện thoại',
                        style: TextStyle(
                            color: Color.fromARGB(255, 0, 21, 37),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      TextField(
                        decoration: InputDecoration(
                          hintText: ' 0976567785',
                          hintStyle: TextStyle(fontSize: 14),
                          contentPadding: EdgeInsets.only(left: 20.0, top: 5),
                        ),
                      )
                    ],
                  ),
                  Divider(
                    height: 0.1,
                    thickness: 1,
                    indent: 1,
                    color: Colors.black,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Padding(padding: EdgeInsets.all(10)),
                      Text(
                        'Mật khẩu đăng nhập',
                        style: TextStyle(
                            color: Color.fromARGB(255, 0, 21, 37),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                          hintText: '**********',
                          hintStyle: TextStyle(fontSize: 14),
                          contentPadding: EdgeInsets.only(left: 20.0, top: 5),
                          suffixIcon: IconButton(
                            onPressed: () {},
                            icon: Icon(Icons.mode_edit_outline),
                          ),
                        ),
                      )
                    ],
                  ),
                  Divider(
                    height: 0.1,
                    thickness: 1,
                    indent: 1,
                    color: Colors.black,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Padding(padding: EdgeInsets.all(10)),
                      Text(
                        'Email',
                        style: TextStyle(
                            color: Color.fromARGB(255, 0, 21, 37),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      TextField(
                        decoration: InputDecoration(
                          hintText: 'Enter your email',
                          hintStyle: TextStyle(fontSize: 14),
                          contentPadding: EdgeInsets.only(left: 20.0, top: 5),
                          suffixIcon: IconButton(
                            onPressed: () {},
                            icon: Icon(Icons.mode_edit_outline),
                          ),
                        ),
                      )
                    ],
                  ),
                  Divider(
                    height: 0.1,
                    thickness: 1,
                    indent: 1,
                    color: Colors.black,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Padding(padding: EdgeInsets.all(10)),
                      Text(
                        'Địa chỉ',
                        style: TextStyle(
                            color: Color.fromARGB(255, 0, 21, 37),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      TextField(
                        decoration: InputDecoration(
                          hintText: 'Nhập địa chỉ của bạn',
                          hintStyle: TextStyle(fontSize: 14),
                          contentPadding: EdgeInsets.only(left: 20.0, top: 5),
                          suffixIcon: IconButton(
                            onPressed: () {},
                            icon: Icon(Icons.mode_edit_outline_rounded),
                          ),
                        ),
                      )
                    ],
                  ),
                  Divider(
                    height: 0.1,
                    thickness: 1,
                    indent: 1,
                    color: Colors.black,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromARGB(255, 245, 219, 70),
                      onPrimary: Colors.white,
                      shadowColor: Colors.greenAccent,
                      elevation: 3,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32.0)),
                      minimumSize: Size(120, 40), //////// HERE
                    ),
                    onPressed: () {},
                    child: Text(
                      'Hoàn Thành',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
