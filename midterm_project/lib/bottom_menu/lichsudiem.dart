import 'package:flutter/material.dart';
import 'package:midterm_project/home_page.dart';
import 'package:midterm_project/bottom_menu/lichsu.dart';

class LichSuDiem extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LichSuDiemState();
  }
}

class LichSuDiemState extends State<LichSuDiem> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          color: Colors.black,
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => LichSu()));
          },
        ),
        title: Text(
          'Lịch sử điểm',
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 15),
              child: Column(
                children: [
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: "Điểm của bạn trong ngày : ",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: "15-3-2022",
                          style: TextStyle(
                              color: Color.fromARGB(255, 255, 23, 23),
                              fontWeight: FontWeight.bold)),
                    ]),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 15),
              child: Column(
                children: [
                  Text(
                    '2.007đ',
                    style: TextStyle(
                      fontSize: 20,
                      color: Color.fromARGB(255, 248, 116, 8),
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            ),
            Divider(
              height: 0.1,
              thickness: 0.5,
              indent: 1,
              color: Colors.black,
            ),
            Container(
              padding: EdgeInsets.only(top: 15),
              child: Column(
                children: [
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: "Điểm của bạn trong ngày: ",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: "15-3-2022",
                          style: TextStyle(
                              color: Color.fromARGB(255, 255, 23, 23),
                              fontWeight: FontWeight.bold)),
                    ]),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 15),
              child: Column(
                children: [
                  Text(
                    '5.150',
                    style: TextStyle(
                      fontSize: 20,
                      color: Color.fromARGB(255, 248, 116, 8),
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            ),
            Divider(
              height: 0.1,
              thickness: 1,
              indent: 1,
              color: Colors.black,
            ),
            Container(
              padding: EdgeInsets.only(top: 15),
              child: Column(
                children: [
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: "Điểm của bạn trong ngày : ",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: "25-4-2022",
                          style: TextStyle(
                              color: Color.fromARGB(255, 255, 23, 23),
                              fontWeight: FontWeight.bold)),
                    ]),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 15),
              child: Column(
                children: [
                  Text(
                    '1.017đ',
                    style: TextStyle(
                      fontSize: 20,
                      color: Color.fromARGB(255, 248, 116, 8),
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
