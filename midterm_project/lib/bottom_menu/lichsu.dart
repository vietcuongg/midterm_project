import 'package:flutter/material.dart';
import 'package:midterm_project/bottom_menu/lichsudiem.dart';
import 'package:midterm_project/home_page.dart';
import 'package:midterm_project/menu/vesinhthietbi.dart';
import 'package:midterm_project/qrcode/qrcode_page.dart';
import 'package:midterm_project/bottom_menu/lichsudonhang.dart';

class LichSu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LichSuPage();
  }
}

class LichSuPage extends State<LichSu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            color: Colors.black,
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => homepage()));
            },
          ),
          title: Text(
            'Lịch sử đơn hàng',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: Container(
            height: MediaQuery.of(context).size.height * 0.75,
            decoration:
                BoxDecoration(color: Color.fromARGB(255, 255, 255, 255)),
            child: SingleChildScrollView(
                child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    padding: const EdgeInsets.symmetric(vertical: 15),
                    child: Column(children: [
                      InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LichSuDonHang()));
                          },
                          child: const ListTile(
                            minLeadingWidth: 0.05,
                            title: Text('Lịch sử đơn hàng',
                                style: TextStyle(
                                    color: Color.fromARGB(210, 0, 0, 0),
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold)),
                            leading: Icon(
                              Icons.shopping_bag_outlined,
                              color: Color.fromARGB(255, 0, 1, 70),
                            ),
                            dense: true,
                            visualDensity: VisualDensity(vertical: -4),
                          )),
                      SizedBox(
                        height: 12,
                      ),
                      Divider(
                        height: 5,
                        thickness: 0.5,
                        indent: 1,
                        color: Colors.black,
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LichSuDiem()));
                          },
                          child: const ListTile(
                            minLeadingWidth: 0.05,
                            title: Text('Lịch sử điểm',
                                style: TextStyle(
                                    color: Color.fromARGB(210, 0, 0, 0),
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold)),
                            leading: Icon(
                              Icons.credit_card,
                              color: Color.fromARGB(255, 0, 1, 70),
                            ),
                            dense: true,
                            visualDensity: VisualDensity(vertical: -4),
                          )),
                      SizedBox(
                        height: 12,
                      ),
                      Divider(
                        height: 5,
                        thickness: 0.5,
                        indent: 1,
                        color: Colors.black,
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => QrcodePage()));
                          },
                          child: const ListTile(
                            minLeadingWidth: 0.05,
                            title: Text('Lịch sử bảo hành - sửa chữa ',
                                style: TextStyle(
                                    color: Color.fromARGB(210, 0, 0, 0),
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold)),
                            leading: Icon(
                              Icons.fire_extinguisher,
                              color: Color.fromARGB(255, 0, 1, 70),
                            ),
                            dense: true,
                            visualDensity: VisualDensity(vertical: -4),
                          )),
                      SizedBox(
                        height: 12,
                      ),
                      Divider(
                        height: 5,
                        thickness: 0.5,
                        indent: 1,
                        color: Colors.black,
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => vesinhthietbiPage()));
                          },
                          child: const ListTile(
                            minLeadingWidth: 0.05,
                            title: Text('Lịch sử đặt lịch vệ sinh thiết bị',
                                style: TextStyle(
                                    color: Color.fromARGB(210, 0, 0, 0),
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold)),
                            leading: Icon(
                              Icons.auto_fix_high,
                              color: Color.fromARGB(255, 0, 1, 70),
                            ),
                            dense: true,
                            visualDensity: VisualDensity(vertical: -4),
                          )),
                      SizedBox(
                        height: 12,
                      ),
                      Divider(
                        height: 5,
                        thickness: 0.5,
                        indent: 1,
                        color: Colors.black,
                      ),
                    ])))));
  }
}
