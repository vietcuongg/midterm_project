import 'package:flutter/material.dart';
import 'package:midterm_project/home_page.dart';
import 'package:midterm_project/bottom_menu/lichsu.dart';

class XemDiemPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return XemDiemPageState();
  }
}

class XemDiemPageState extends State<XemDiemPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          color: Colors.black,
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => homepage()));
          },
        ),
        title: Text(
          'Tích Điểm',
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 15),
              child: Column(
                children: [
                  Text(
                    'Tổng điểm hiện tại của bạn là:',
                    style: TextStyle(
                        fontSize: 20, color: Color.fromARGB(255, 0, 32, 58)),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 15),
              child: Column(
                children: [
                  Text(
                    '19.05102',
                    style: TextStyle(
                      fontSize: 20,
                      color: Color.fromARGB(255, 248, 116, 8),
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
