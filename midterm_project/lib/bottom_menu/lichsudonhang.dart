import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:midterm_project/bottom_menu/lichsu.dart';
import 'package:midterm_project/bottom_menu/xemdiem.dart';
import 'package:midterm_project/home_page.dart';

class LichSuDonHang extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LichSuDonHangState();
  }
}

class LichSuDonHangState extends State<LichSuDonHang> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          color: Colors.black,
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => LichSu()));
          },
        ),
        title: Text(
          'Lịch sử đơn hàng',
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          Container(
            padding: EdgeInsets.all(10),
            height: 250,
            color: Color.fromARGB(255, 218, 215, 215),
            child: Column(
              children: [
                Row(
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Đơn hàng: ",
                            style: TextStyle(
                                color: Color.fromARGB(221, 0, 15, 150),
                                fontWeight: FontWeight.bold)),
                        TextSpan(
                            text: "Bách Hóa Xanh",
                            style: TextStyle(
                                color: Color.fromARGB(255, 21, 124, 243),
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                    SizedBox(
                      width: 100,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.green,
                        onPrimary: Colors.white,
                        shadowColor: Colors.greenAccent,
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32.0)),
                        minimumSize: Size(100, 20), //////// HERE
                      ),
                      onPressed: () {},
                      child: Text('Đã giao'),
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Column(
                  children: [
                    Row(
                      children: [
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(children: <TextSpan>[
                            TextSpan(
                                text: "Tổng tiền: ",
                                style: TextStyle(color: Colors.black87)),
                            TextSpan(
                                text: "250.000đ",
                                style: TextStyle(
                                    color: Color.fromARGB(255, 255, 3, 3),
                                    fontWeight: FontWeight.bold)),
                          ]),
                        ),
                      ],
                    )
                  ],
                ),
                Row(
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Mua lúc: ",
                            style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: "15:11",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                    SizedBox(
                      width: 180,
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Giao lúc: ",
                            style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: "16:00",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                  ],
                ),
                Row(
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Ngày: ",
                            style: TextStyle(
                              color: Colors.black,
                            )),
                        TextSpan(
                            text: "15-3-2022",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Icon(Icons.local_shipping),
                    SizedBox(
                      width: 50,
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Ngày: ",
                            style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: "15-3-2022",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                DottedBorder(
                    color: Colors.orange,
                    strokeWidth: 2,
                    dashPattern: [
                      5,
                      5,
                    ],
                    child: Container(
                        color: Color.fromARGB(255, 241, 209, 121),
                        padding: const EdgeInsets.only(top: 1.0, left: 5),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                      text: "Điểm tích lũy ",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ]),
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                        text: "2.007đ ",
                                        style: TextStyle(
                                            color: Color.fromARGB(
                                                255, 253, 12, 12),
                                            fontWeight: FontWeight.bold)),
                                  ]),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Icon(
                                  Icons.arrow_forward_rounded,
                                  color: Color.fromARGB(255, 15, 75, 17),
                                ),
                                SizedBox(
                                  width: 50,
                                ),
                                ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary:
                                          Color.fromARGB(255, 255, 255, 255),
                                      onPrimary: Colors.white,
                                      shadowColor: Colors.greenAccent,
                                      elevation: 5,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(30),
                                        ),
                                      ),
                                      minimumSize: Size(20, 30), //////// HERE
                                    ),
                                    onPressed: () {},
                                    child: Expanded(
                                      child: Text(
                                        'Chi tiết đơn hàng',
                                        maxLines: 4,
                                        style: TextStyle(
                                          color: Colors.blueAccent,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 10,
                                        ),
                                      ),
                                    )),
                              ],
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Color.fromARGB(255, 23, 121, 26),
                                onPrimary: Colors.white,
                                shadowColor: Colors.greenAccent,
                                elevation: 3,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                minimumSize: Size(120, 30),
                              ),
                              onPressed: () {},
                              child: Text('Đã tích'),
                            )
                          ],
                        )))
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.all(10),
            height: 250,
            color: Color.fromARGB(255, 218, 215, 215),
            child: Column(
              children: [
                Row(
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Đơn hàng: ",
                            style: TextStyle(
                                color: Color.fromARGB(221, 0, 15, 150),
                                fontWeight: FontWeight.bold)),
                        TextSpan(
                            text: "Bách Hóa Xanh",
                            style: TextStyle(
                                color: Color.fromARGB(255, 21, 124, 243),
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                    SizedBox(
                      width: 100,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.green,
                        onPrimary: Colors.white,
                        shadowColor: Colors.greenAccent,
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32.0)),
                        minimumSize: Size(100, 20), //////// HERE
                      ),
                      onPressed: () {},
                      child: Text('Đã giao'),
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Column(
                  children: [
                    Row(
                      children: [
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(children: <TextSpan>[
                            TextSpan(
                                text: "Tổng tiền: ",
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold)),
                            TextSpan(
                                text: "567.000đ",
                                style: TextStyle(
                                    color: Color.fromARGB(255, 255, 3, 3),
                                    fontWeight: FontWeight.bold)),
                          ]),
                        ),
                      ],
                    )
                  ],
                ),
                Row(
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Mua lúc: ",
                            style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: "10:15",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                    SizedBox(
                      width: 180,
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Giao lúc: ",
                            style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: "11:00",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                  ],
                ),
                Row(
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Ngày: ",
                            style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: "19-4-2022",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Icon(Icons.local_shipping),
                    SizedBox(
                      width: 50,
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Ngày: ",
                            style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: "19-4-2022",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                DottedBorder(
                    color: Colors.orange,
                    strokeWidth: 2,
                    dashPattern: [
                      5,
                      5,
                    ],
                    child: Container(
                        color: Color.fromARGB(255, 241, 209, 121),
                        padding: const EdgeInsets.only(top: 1.0, left: 5),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                        text: "Điểm tích lũy ",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold)),
                                  ]),
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                        text: "5.150đ ",
                                        style: TextStyle(
                                            color: Color.fromARGB(
                                                255, 253, 12, 12),
                                            fontWeight: FontWeight.bold)),
                                  ]),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Icon(Icons.arrow_forward_rounded,
                                    color: Color.fromARGB(255, 37, 90, 48)),
                                SizedBox(
                                  width: 50,
                                ),
                                ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary:
                                          Color.fromARGB(255, 255, 255, 255),
                                      onPrimary: Colors.white,
                                      shadowColor: Colors.greenAccent,
                                      elevation: 5,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(30),
                                        ),
                                      ),
                                      minimumSize: Size(20, 30), //////// HERE
                                    ),
                                    onPressed: () {},
                                    child: Expanded(
                                      child: Text(
                                        'Chi tiết đơn hàng',
                                        maxLines: 4,
                                        style: TextStyle(
                                          color: Colors.blueAccent,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 10,
                                        ),
                                      ),
                                    )),
                              ],
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Color.fromARGB(255, 23, 121, 26),
                                onPrimary: Colors.white,
                                shadowColor: Colors.greenAccent,
                                elevation: 3,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                minimumSize: Size(120, 30), //////// HERE
                              ),
                              onPressed: () {},
                              child: Text('Đã tích'),
                            )
                          ],
                        )))
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.all(10),
            height: 250,
            color: Color.fromARGB(255, 218, 215, 215),
            child: Column(
              children: [
                Row(
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Đơn hàng: ",
                            style: TextStyle(
                                color: Color.fromARGB(221, 0, 15, 150),
                                fontWeight: FontWeight.bold)),
                        TextSpan(
                            text: "Bách Hóa Xanh",
                            style: TextStyle(
                                color: Color.fromARGB(255, 21, 124, 243),
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                    SizedBox(
                      width: 100,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.green,
                        onPrimary: Colors.white,
                        shadowColor: Colors.greenAccent,
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32.0)),
                        minimumSize: Size(100, 20), //////// HERE
                      ),
                      onPressed: () {},
                      child: Text('Đã giao'),
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Column(
                  children: [
                    Row(
                      children: [
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(children: <TextSpan>[
                            TextSpan(
                              text: "Tổng tiền: ",
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.bold),
                            ),
                            TextSpan(
                                text: "100.000đ",
                                style: TextStyle(
                                    color: Color.fromARGB(255, 255, 3, 3),
                                    fontWeight: FontWeight.bold)),
                          ]),
                        ),
                      ],
                    )
                  ],
                ),
                Row(
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Mua lúc: ",
                            style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: "6:17",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                    SizedBox(
                      width: 180,
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Giao lúc: ",
                            style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: "6:17",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                  ],
                ),
                Row(
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Ngày: ",
                            style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: "25-4-2022",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Icon(Icons.local_shipping),
                    SizedBox(
                      width: 50,
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Ngày: ",
                            style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: "25-4-2022",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                      ]),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                DottedBorder(
                    color: Colors.orange,
                    strokeWidth: 2,
                    dashPattern: [
                      5,
                      5,
                    ],
                    child: Container(
                        color: Color.fromARGB(255, 241, 209, 121),
                        padding: const EdgeInsets.only(top: 1.0, left: 5),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                        text: "Điểm tích lũy ",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold)),
                                  ]),
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                        text: "1.017đ ",
                                        style: TextStyle(
                                            color: Color.fromARGB(
                                                255, 253, 12, 12),
                                            fontWeight: FontWeight.bold)),
                                  ]),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Icon(Icons.arrow_forward_rounded,
                                    color: Color.fromARGB(255, 27, 63, 35)),
                                SizedBox(
                                  width: 50,
                                ),
                                ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary:
                                          Color.fromARGB(255, 255, 255, 255),
                                      onPrimary: Colors.white,
                                      shadowColor: Colors.greenAccent,
                                      elevation: 5,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(30),
                                        ),
                                      ),
                                      minimumSize: Size(20, 30), //////// HERE
                                    ),
                                    onPressed: () {},
                                    child: Expanded(
                                      child: Text(
                                        'Chi tiết đơn hàng',
                                        maxLines: 4,
                                        style: TextStyle(
                                          color: Colors.blueAccent,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 10,
                                        ),
                                      ),
                                    )),
                              ],
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Color.fromARGB(255, 23, 121, 26),
                                onPrimary: Colors.white,
                                shadowColor: Colors.greenAccent,
                                elevation: 3,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                minimumSize: Size(120, 30), //////// HERE
                              ),
                              onPressed: () {},
                              child: Text('Đã tích'),
                            )
                          ],
                        )))
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
