import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:midterm_project/app.dart';
import 'package:midterm_project/bottom_menu/taikhoan.dart';
import 'package:midterm_project/menu/gopykhieunai.dart';

import 'package:midterm_project/menu/navbar.dart';
import 'package:midterm_project/menu/notification_page.dart';
import 'package:midterm_project/menu/theodoidonhang.dart';
import 'package:midterm_project/menu/vesinhthietbi.dart';
import 'package:midterm_project/models/appbanner.dart';
import 'package:midterm_project/qrcode/qrcode_page.dart';
import 'package:midterm_project/navigationmenu/navbar.dart';

class wellcomepage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return wellcomepageState();
  }
}

class wellcomepageState extends State<wellcomepage> {
  ScrollController _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBar(),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 15,
        title: Text(
          'Xin chào bạn',
          style: TextStyle(
            fontSize: 15,
            color: Colors.transparent, // Step 2 SEE HERE
            shadows: [
              Shadow(offset: Offset(0, -10), color: Colors.black)
            ], // Step 3 SEE HERE
            decoration: TextDecoration.underline,
            decorationThickness: 1.5,

            decorationColor: Color.fromARGB(255, 254, 56, 56),
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.notifications_none,
              color: Colors.black,
            ),
            onPressed: () {
              //nNOTFICATIONS//
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => NotificationPage()));
            },
          )
        ],
        flexibleSpace: Container(
            decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
          gradient: LinearGradient(colors: [
            Colors.orange,
            Colors.yellow,
          ], begin: Alignment.bottomRight, end: Alignment.topLeft),
        )),
        bottom: PreferredSize(
            preferredSize: Size.fromHeight(80),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Container(
                        padding: const EdgeInsets.only(left: 15, bottom: 25),
                        child: CircleAvatar(
                          backgroundColor: Color.fromARGB(255, 255, 160, 58),
                          radius: 25,
                          foregroundColor: Colors.black,
                          child: IconButton(
                            icon: Icon(
                              Icons.person,
                              color: Colors.black,
                            ),
                            iconSize: 20.0,
                            padding: EdgeInsets.all(0),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => taikhoanpage()));
                            },
                          ),
                        ),
                      )),
                  Expanded(
                    flex: 3,
                    child: Container(
                      alignment: Alignment.bottomLeft,
                      padding: EdgeInsets.only(left: 15, bottom: 25),
                      child: Column(
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Row(
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        setState(() {
                                          backgroundColor:
                                          Colors.red;
                                        });
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    QrcodePage()));
                                      },
                                      child: const Text(
                                        'Hiện tại chưa có điểm',
                                        style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 39, 73),
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Roboto'),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    QrcodePage()));
                                      },
                                      child: const Text(
                                        'hoặc đã sử dụng hết điểm',
                                        style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 39, 73),
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Roboto'),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: Alignment.bottomLeft,
                      padding: EdgeInsets.only(bottom: 25),
                      child: Column(
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 0.1),
                                child: Row(
                                  children: [
                                    IconButton(
                                      icon: Icon(
                                        Icons.double_arrow_outlined,
                                        size: 15,
                                        color: Color.fromARGB(255, 0, 39, 73),
                                      ),
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    QrcodePage()));
                                      },
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      alignment: Alignment.bottomLeft,
                      padding: EdgeInsets.only(left: 15, bottom: 25),
                      child: Column(
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Row(
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        setState(() {
                                          backgroundColor:
                                          Colors.red;
                                        });
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    QrcodePage()));
                                      },
                                      child: const Text(
                                        'Mã định danh ',
                                        style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 39, 73),
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    QrcodePage()));
                                      },
                                      child: const Text(
                                        '738AQ9J ',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 151, 2, 2),
                                          fontSize: 18,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )),
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.all(15),
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black, width: 2),
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 15, top: 60),
                    ),
                    Expanded(
                        child: Text(
                      "Trải nghiệm các tiện ích và hỗ trợ nhanh dành cho bạn.",
                      style: TextStyle(
                          fontSize: 18.0,
                          color: Color.fromARGB(255, 0, 60, 90),
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.bold),
                    ))
                  ],
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.33,
                  child: GridView(
                    shrinkWrap: true,
                    primary: true,
                    padding: const EdgeInsets.all(16),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 18,
                      mainAxisSpacing: 10,
                      mainAxisExtent: 60,
                    ),
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: [
                            MaterialButton(
                              height: 35,
                              minWidth: 10,
                              color: Colors.orange,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0)),
                              child: Icon(
                                Icons.badge_outlined,
                                color: Colors.black,
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            theodoidonhangPage()));
                              },
                            ),
                            Padding(padding: EdgeInsets.only(left: 10)),
                            Expanded(
                              child: Text(
                                'Theo goi don hang cho giao',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            MaterialButton(
                              height: 35,
                              minWidth: 10,
                              color: Color.fromARGB(255, 2, 76, 145),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0)),
                              child: Icon(
                                Icons.auto_fix_high,
                                color: Colors.white,
                              ),
                              onPressed: () {},
                            ),
                            Padding(padding: EdgeInsets.only(left: 10)),
                            Expanded(
                              child: Text(
                                'Hỗ trợ bảo hành, sửa chữa',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            MaterialButton(
                              height: 35,
                              minWidth: 10,
                              color: Color.fromARGB(255, 28, 243, 214),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0)),
                              child: Icon(
                                Icons.message_outlined,
                                color: Colors.black,
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            gopukhieunaiPage()));
                              },
                            ),
                            Padding(padding: EdgeInsets.only(left: 10)),
                            Expanded(
                              child: Text(
                                'Góp ý khiếu nại',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            MaterialButton(
                              height: 35,
                              minWidth: 10,
                              color: Color.fromARGB(212, 245, 102, 238),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0)),
                              child: Icon(
                                Icons.perm_phone_msg,
                                color: Colors.black,
                              ),
                              onPressed: () {},
                            ),
                            Padding(padding: EdgeInsets.only(left: 10)),
                            Expanded(
                              child: Text(
                                'Hỗ trợ kĩ thuật trực tuyến',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            MaterialButton(
                              height: 35,
                              minWidth: 10,
                              color: Color.fromARGB(255, 94, 238, 118),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0)),
                              child: Icon(
                                Icons.calendar_today,
                                color: Colors.black,
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            vesinhthietbiPage()));
                              },
                            ),
                            Padding(padding: EdgeInsets.only(left: 10)),
                            Expanded(
                              child: Text(
                                'Đặt lịch vệ sinh thiết bị',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            MaterialButton(
                              height: 35,
                              minWidth: 10,
                              color: Color.fromARGB(255, 231, 58, 58),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0)),
                              child: Icon(
                                Icons.card_giftcard_sharp,
                                color: Colors.black,
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            NotificationPage()));
                              },
                            ),
                            Padding(padding: EdgeInsets.only(left: 10)),
                            Expanded(
                              child: Text(
                                'Qùa của tôi',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 1,
                ),
                DottedBorder(
                  color: Colors.orange,
                  strokeWidth: 1.25,
                  dashPattern: [
                    5,
                    5,
                  ],
                  child: Container(
                    color: Color.fromARGB(223, 253, 255, 148),
                    width: MediaQuery.of(context).size.width * 0.65,
                    height: 30,
                    padding: const EdgeInsets.all(7.0),
                    child: const Text(
                      "Tổng đài hỗ trợ(gọi miễn phí): 1800.1060",
                      style: TextStyle(
                          fontSize: 12.0, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 2.0),
            height: 80,
            decoration:
                BoxDecoration(color: Color.fromARGB(255, 255, 255, 255)),
            child: PageView.builder(
              itemCount: appBannerList.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10.0),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(appBannerList[index].imgUrl),
                          fit: BoxFit.cover)),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
