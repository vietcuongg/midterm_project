import 'package:flutter/material.dart';
import 'package:midterm_project/models/intro_data.dart';
import 'package:midterm_project/models/size_configs.dart';
import 'package:midterm_project/models/styles.dart';
import 'SignIn_page.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return IntroPageState();
  }
}

class IntroPageState extends State<IntroPage> {
  int currentPage = 0;

  PageController _pageController = PageController(initialPage: 0);

  //Make a change screen//
  AnimatedContainer doIndicator(index) {
    return AnimatedContainer(
      margin: EdgeInsets.all(1.0),
      duration: Duration(milliseconds: 400),
      height: 14,
      width: 12,
      decoration: BoxDecoration(
        color: currentPage == index
            ? Color.fromARGB(255, 255, 221, 0)
            : Color.fromARGB(104, 83, 80, 80),
        shape: BoxShape.circle,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    double sizeH = SizeConfig.blockSizeH!;
    double sizeV = SizeConfig.blockSizeV!;

    return Scaffold(
      backgroundColor: Color.fromARGB(255, 255, 255, 255),
      body: SafeArea(
          child: Column(
        children: [
          Expanded(
              flex: 5,
              child: PageView.builder(
                  controller: _pageController,
                  onPageChanged: (value) {
                    setState(() {
                      currentPage = value;
                    });
                  },
                  //select the list image , set size
                  itemCount: intro_dataContents.length,
                  itemBuilder: (context, index) => Column(children: [
                        SizedBox(
                          height: sizeV * 5,
                        ),
                        Text(
                          intro_dataContents[index].title,
                          style: tTtitle,
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: sizeV * 5,
                        ),
                        Container(
                          height: sizeV * 50,
                          child: Image.asset(
                            intro_dataContents[index].image,
                            fit: BoxFit.contain,
                          ),
                        ),
                        SizedBox(
                          height: sizeV * 5,
                        ),
                        //Set text 'MY QUA TANG VIP'
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(style: bodyText1, children: [
                            TextSpan(
                              text: 'MY QUA TANG VIP, ',
                              style: TextStyle(
                                  color: Color.fromARGB(255, 255, 230, 0),
                                  fontWeight: FontWeight.bold),
                            ),
                          ]),
                        ),
                        SizedBox(
                          height: sizeV * 5,
                        ),
                      ]))),
          //next skip //
          Expanded(
            flex: 1,
            child: Column(
              children: [
                currentPage == intro_dataContents.length - 1
                    ? StartedButton(
                        buttonName: 'Dang Nhap',
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignInPage()));
                        },
                        bgcolor: Color.fromARGB(255, 255, 232, 57),
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          IntroNavBtn(
                            name: 'skip',
                            onPressed: () {},
                          ),
                          Row(
                            children: List.generate(intro_dataContents.length,
                                (index) => doIndicator(index)),
                          ),
                          IntroNavBtn(
                              name: 'Next',
                              onPressed: () {
                                _pageController.nextPage(
                                  duration: Duration(milliseconds: 400),
                                  curve: Curves.easeInOut,
                                );
                              })
                        ],
                      ),
              ],
            ),
          )
        ],
      )),
    );
  }
}

class StartedButton extends StatelessWidget {
  const StartedButton({
    Key? key,
    required this.buttonName,
    required this.onPressed,
    required this.bgcolor,
  }) : super(key: key);
  final String buttonName;
  final VoidCallback onPressed;
  final Color bgcolor;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: SizedBox(
        height: 40,
        width: 200,
        child: TextButton(
          onPressed: onPressed,
          child: Text(
            'Dang Nhap',
            style: bodyText1,
          ),
          style: TextButton.styleFrom(backgroundColor: bgcolor),
        ),
      ),
    );
  }
}

class IntroNavBtn extends StatelessWidget {
  const IntroNavBtn({
    Key? key,
    required this.name,
    required this.onPressed,
  }) : super(key: key);
  final String name;
  final VoidCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onPressed,
        borderRadius: BorderRadius.circular(10),
        splashColor: Color.fromARGB(237, 254, 254, 254),
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            name,
            style: bodyText1,
          ),
        ));
  }
}
