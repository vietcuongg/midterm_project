import 'package:flutter/material.dart';

import 'package:midterm_project/home_page.dart';
import 'package:midterm_project/screens/password_page.dart';

class SignInPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SignInPageState();
  }
}

class SignInPageState extends State<SignInPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          children: [
            Container(
              height: 150,
              width: double.infinity,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg1.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
              padding: EdgeInsets.only(top: 70),
              child: Text(
                'Đăng Nhập',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              color: Color.fromARGB(255, 255, 255, 255),
              margin: EdgeInsets.all(0),
              child: Column(
                children: [
                  Image.asset(
                    'assets/images/user.png',
                    width: 50,
                    height: 50,
                  ),
                  const Padding(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "XIN CHAO",
                      style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
                    ),
                  ),
                  const TextField(
                    decoration: InputDecoration(
                        labelText: "Nhập số điện thoại của bạn:",
                        labelStyle: TextStyle(fontSize: 15),
                        filled: true),
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  MaterialButton(
                    minWidth: double.infinity,
                    height: 50,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PasswordPage()));
                    },
                    color: Colors.yellow,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40)),
                    child: Text(
                      "Dang Nhap",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
