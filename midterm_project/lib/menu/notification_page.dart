import 'package:flutter/material.dart';

import '../home_page.dart';

class NotificationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationPageState();
  }
}

class NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            color: Colors.black,
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => homepage()));
            },
          ),
          title: Text(
            'Lịch sử điểm',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: Container(
          color: Color.fromARGB(255, 218, 213, 213),
          height: MediaQuery.of(context).size.height * 1,
          padding: EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Container(
              color: Color.fromARGB(255, 255, 255, 255),
              height: 250,
              child: Column(
                children: const [
                  ListTile(
                    minLeadingWidth: 0.05,
                    title: Text('Bách hóa xanh -Tích điểm',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.bold)),
                    subtitle: Text('19:59-19/04/2022'),
                    leading: Icon(
                      Icons.watch_later_outlined,
                      color: Colors.black,
                    ),
                    trailing: Text('+2.000đ'),
                    dense: true,
                    visualDensity: VisualDensity(vertical: -4),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  ListTile(
                    minLeadingWidth: 0.05,
                    title: Text('Bách hóa xanh -Tích điểm',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.bold)),
                    subtitle: Text('12:59 -18/04/2022'),
                    leading: Icon(
                      Icons.watch_later_outlined,
                      color: Colors.black,
                    ),
                    trailing: Text('+1.488đ'),
                    dense: true,
                    visualDensity: VisualDensity(vertical: -4),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  ListTile(
                    minLeadingWidth: 0.05,
                    title: Text('Bách hóa xanh -Tích điểm',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.bold)),
                    subtitle: Text('21:00 -17/04/2022'),
                    leading: Icon(
                      Icons.watch_later_outlined,
                      color: Colors.black,
                    ),
                    trailing: Text('+2.148đ'),
                    dense: true,
                    visualDensity: VisualDensity(vertical: -4),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  ListTile(
                    minLeadingWidth: 0.05,
                    title: Text('Bách hóa xanh -Tích điểm',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.bold)),
                    subtitle: Text('18:04-15/04/2022'),
                    leading: Icon(
                      Icons.watch_later_outlined,
                      color: Colors.black,
                    ),
                    trailing: Text('+1.209đ'),
                    dense: true,
                    visualDensity: VisualDensity(vertical: -4),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
