import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:midterm_project/home_page.dart';

class theodoidonhangPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return theodoidonhangState();
  }
}

class theodoidonhangState extends State<theodoidonhangPage> {
  @override
  Widget build(BuildContext context) {
    debugShowCheckedModeBanner:
    false;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            color: Colors.black,
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => homepage()));
            },
          ),
          title: Text(
            'THEO DÕI ĐƠN HÀNG',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: Container(
          child: Column(
            children: [
              Padding(padding: EdgeInsets.all(15)),
              Row(
                children: [
                  Padding(padding: EdgeInsets.all(15)),
                  Text(
                    'Hiện tại bạn chưa có danh sách đơn hàng',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 0, 35, 63)),
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
