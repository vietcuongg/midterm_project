import 'dart:math';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:midterm_project/home_page.dart';

class vesinhthietbiPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return vesinhthietbiPageState();
  }
}

class vesinhthietbiPageState extends State<vesinhthietbiPage> {
  bool value = false;
  @override
  Widget build(BuildContext context) {
    debugShowCheckedModeBanner:
    false;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            color: Colors.black,
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => homepage()));
            },
          ),
          title: Text(
            'Đặt lịch vệ sinh thiết bị',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 170,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: [
                      Column(
                        children: [
                          Padding(padding: EdgeInsets.all(10)),
                          DottedBorder(
                            color: Colors.orange,
                            strokeWidth: 1.25,
                            dashPattern: [
                              5,
                              5,
                            ],
                            child: Container(
                                color: Color.fromARGB(223, 253, 255, 148),
                                height: 80,
                                width: 350,
                                padding: const EdgeInsets.all(5.0),
                                child: (Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'Bạn cần vệ sinh thiết bị gì? Vui lòng xem báo giá và chọn thiết bị từ danh sách bên dưới',
                                        maxLines: 5,
                                      ),
                                    ),
                                  ],
                                ))),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Bảng báo giá thiết bị cần vệ sinh',
                            style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Divider(
                  height: 0.1,
                  thickness: 0.5,
                  indent: 1,
                  color: Colors.black,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Column(children: [
                    Row(
                      children: [
                        Icon(Icons.airplane_ticket_outlined),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Máy Lạnh',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 0, 41, 75)),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Checkbox(value: value, onChanged: (value) {}),
                        Text(
                          'Vệ sinh máy lạnh từ 1 HP - 1.5 HP',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 2, 2, 2)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Padding(padding: EdgeInsets.only(left: 20)),
                        Text(
                          'Chọn số lượng',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 165, 165, 165)),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Icon(
                          Icons.plus_one_sharp,
                          size: 30,
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Text(
                          '200.000đ',
                          style: TextStyle(color: Colors.red),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    DottedBorder(
                      color: Colors.orange,
                      strokeWidth: 1.25,
                      dashPattern: [
                        5,
                        5,
                      ],
                      child: Container(
                          color: Color.fromARGB(223, 253, 255, 148),
                          height: 30,
                          padding: const EdgeInsets.all(5.0),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(children: <TextSpan>[
                              TextSpan(
                                  text: "Bơm thêm gas có:",
                                  style: TextStyle(color: Colors.black87)),
                              TextSpan(
                                  text: " 200.000đ/máy",
                                  style: TextStyle(
                                      color: Color.fromARGB(255, 248, 0, 0),
                                      fontWeight: FontWeight.bold)),
                            ]),
                          )),
                    ),
                  ]),
                ),
                Divider(
                  height: 0.1,
                  thickness: 0.5,
                  indent: 1,
                  color: Colors.black,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Column(children: [
                    Row(
                      children: [
                        Icon(Icons.airplane_ticket_outlined),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Máy giặt',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 0, 41, 75)),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Checkbox(
                            value: value,
                            onChanged: (value) {
                              setState(() {
                                this.value = value!;
                              });
                            }),
                        Text(
                          'Dịc vụ vệ sinh máy giặt cửa trên',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 2, 2, 2)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Padding(padding: EdgeInsets.only(left: 20)),
                        Text(
                          'Chọn số lượng',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 165, 165, 165)),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Icon(
                          Icons.plus_one_sharp,
                          size: 30,
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Text(
                          '500.000đ',
                          style: TextStyle(color: Colors.red),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    DottedBorder(
                      color: Colors.orange,
                      strokeWidth: 1.25,
                      dashPattern: [
                        5,
                        5,
                      ],
                      child: Container(
                          color: Color.fromARGB(223, 253, 255, 148),
                          height: 30,
                          padding: const EdgeInsets.all(5.0),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(children: <TextSpan>[
                              TextSpan(
                                  text: "Thay đường nước:",
                                  style: TextStyle(color: Colors.black87)),
                              TextSpan(
                                  text: " 250.000đ/máy",
                                  style: TextStyle(
                                      color: Color.fromARGB(255, 248, 0, 0),
                                      fontWeight: FontWeight.bold)),
                            ]),
                          )),
                    ),
                  ]),
                ),
                Divider(
                  height: 0.1,
                  thickness: 0.5,
                  indent: 1,
                  color: Colors.black,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Column(children: [
                    Row(
                      children: [
                        Icon(Icons.airplane_ticket_outlined),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Quạt',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 0, 41, 75)),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Checkbox(
                            value: value,
                            onChanged: (value) {
                              setState(() {
                                this.value = value!;
                              });
                            }),
                        Text(
                          'Vệ sinh quạt điều hòa/Hơi nước',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 2, 2, 2)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Padding(padding: EdgeInsets.only(left: 20)),
                        Text(
                          'Chọn số lượng',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 165, 165, 165)),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Icon(
                          Icons.plus_one_sharp,
                          size: 30,
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Text(
                          '150.000đ',
                          style: TextStyle(color: Colors.red),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(
                      height: 1,
                      thickness: 0.5,
                      color: Colors.black,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Checkbox(
                            value: value,
                            onChanged: (value) {
                              setState(() {
                                this.value = value!;
                              });
                            }),
                        Text(
                          'Vệ sinh quạt điều đứng/treo',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 2, 2, 2)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Padding(padding: EdgeInsets.only(left: 20)),
                        Text(
                          'Chọn số lượng',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 165, 165, 165)),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Icon(
                          Icons.plus_one_sharp,
                          size: 30,
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Text(
                          '100.000đ',
                          style: TextStyle(color: Colors.red),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    DottedBorder(
                      color: Colors.orange,
                      strokeWidth: 1.25,
                      dashPattern: [
                        5,
                        5,
                      ],
                      child: Container(
                          color: Color.fromARGB(223, 253, 255, 148),
                          height: 30,
                          padding: const EdgeInsets.all(5.0),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(children: <TextSpan>[
                              TextSpan(
                                  text: "Thay đầu điện:",
                                  style: TextStyle(color: Colors.black87)),
                              TextSpan(
                                  text: " 70.000đ/máy",
                                  style: TextStyle(
                                      color: Color.fromARGB(255, 248, 0, 0),
                                      fontWeight: FontWeight.bold)),
                            ]),
                          )),
                    ),
                  ]),
                ),
              ],
            ),
          ),
        ));
  }
}
