import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:midterm_project/home_page.dart';

class gopukhieunaiPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return gopukhieunaiPageState();
  }
}

class gopukhieunaiPageState extends State<gopukhieunaiPage> {
  TextEditingController textarea = TextEditingController();
  @override
  Widget build(BuildContext context) {
    debugShowCheckedModeBanner:
    false;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            color: Colors.black,
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => homepage()));
            },
          ),
          title: const Text(
            ' Tạo góp ý - khiếu nại   ',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: Container(
          constraints: const BoxConstraints(
            maxHeight: double.infinity,
          ),
          child: Column(
            children: [
              const Padding(padding: EdgeInsets.all(15)),
              Row(
                children: const [
                  Padding(padding: EdgeInsets.all(15)),
                  Text(
                    'Nội dung góp ý -  khiếu nại',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 0, 35, 63)),
                  ),
                ],
              ),
              Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(20),
                  child: Column(children: [
                    TextField(
                      controller: textarea,
                      keyboardType: TextInputType.multiline,
                      maxLines: 5,
                      decoration: const InputDecoration(
                          hintText: "Enter Remarks",
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1, color: Colors.redAccent))),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: const Size(250, 40),
                          primary: Colors.yellow,
                          shadowColor: Colors.orange,
                        ),
                        onPressed: () {
                          print(textarea.text);
                        },
                        child: const Text(
                          "Gửi",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ))
                  ]))
            ],
          ),
        ));
  }
}
