import 'package:flutter/material.dart';
import './size_configs.dart';

Color firstColor = Color(0xffC9d46);
Color secondColor = Color.fromARGB(255, 0, 0, 0);

final tTtitle = TextStyle(
  fontFamily: 'Klasik',
  fontSize: 20,
);

final bodyText1 = TextStyle(
  color: secondColor,
  fontSize: SizeConfig.blockSizeH! * 4.5,
  fontWeight: FontWeight.bold,
);
