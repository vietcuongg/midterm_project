class intro_data {
  final String title;
  final String image;

  intro_data({
    required this.title,
    required this.image,
  });
}

List<intro_data> intro_dataContents = [
  intro_data(
    title: 'Dù bạn mua đó, mua đây. Chỉ cần mở app là ngay tích tiên',
    image: 'assets/images/intro1.jpg',
  ),
  intro_data(
    title: 'Ô kìa, tích thật nhiều tiền. Chỉ cần nhập số là khiêng em liền',
    image: 'assets/images/intro2.jpg',
  ),
  intro_data(
    title: 'Nhìn tiện ích, thấy thiệt mê. ',
    image: 'assets/images/intro3.jpg',
  ),
  intro_data(
    title: 'Chúc bạn tích thật nhiều điểm, đổi thật nhiều quà.',
    image: 'assets/images/intro4.jpg',
  ),
];
