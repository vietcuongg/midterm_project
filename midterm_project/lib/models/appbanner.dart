class AppBanner {
  final String imgUrl;

  AppBanner(this.imgUrl);
}

List<AppBanner> appBannerList = [
  AppBanner(
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRm8nURfo-KOAgk0bocjY1w4s6dZ5jfRoXhBg&usqp=CAU'),
  AppBanner(
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQnd33JRHAd2IPImlXf_NB78v3XerN4Mh6pzA&usqp=CAU'),
  AppBanner(
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRkpFPHHTJTg6-kQsRHGJZvHQ3Fc2_6CHJjoA&usqp=CAU'),
  AppBanner(
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMNBiUSl04mktApEF6PyG_F8HVVN15A80NIg&usqp=CAU'),
  AppBanner(
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFdfS3bGTmz1gW_ZXeNyWphjNq63JPw9PzxQ&usqp=CAU'),
];
